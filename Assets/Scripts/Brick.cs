﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Poramid {
	public class Brick:MonoBehaviour {
		public BrickShape shape { get; private set; }
		public string texture { get; private set; }

		public Transform tr { get; private set; }
		public Rigidbody2D rb { get; private set; }
		public Transform sprite { get; private set; }
		public Renderer rend { get; private set; }
		public PolygonCollider2D col { get; private set; }

		int waitIgnore;
		int waitDelta;
		bool stop;

		public bool released { get; private set; }

		public static Texture2D[] textures {
			get {
				GetTextures();
				return _textures;
			}
		}
		public static Texture2D textureDef {
			get {
				GetTextures();
				return _textureDef;
			}
		}
		public static Dictionary<string,Texture2D> textureByName {
			get {
				GetTextures();
				return _textureByName;
			}
		}
		static Texture2D[] _textures = null;
		static Texture2D _textureDef = null;
		static Dictionary<string,Texture2D> _textureByName = null;
		static void GetTextures() {
			if (_textures != null) return;
			var texturesList = new List<Texture2D>();
			var tex = Resources.LoadAll<Texture2D>("sprites/bricks");
			_textureByName = new Dictionary<string,Texture2D>();
			for (int a = 0; a < tex.Length; a++) {
				if (tex[a] != null) {
					_textureByName.Add(tex[a].name,tex[a]);
					if (tex[a].name == "default") {
						_textureDef = tex[a];
						texturesList.Insert(0,tex[a]);
					} else {
						texturesList.Add(tex[a]);
					}
				}
			}
			_textures = texturesList.ToArray();
		}

		static int brickLayerIndex = -1;

		void Awake() {
			tr = transform;
			rb = GetComponent<Rigidbody2D>();
			sprite = tr.Find("sprite");
			rend = sprite.GetComponent<Renderer>();
			col = GetComponent<PolygonCollider2D>();
			waitIgnore = 0;
			waitDelta = 0;
			stop = false;
			if (brickLayerIndex < 0) {
				brickLayerIndex = LayerMask.NameToLayer("Brick");
			}
			rend.material.color = new Color(.25f,.25f,.25f,1);
			Level.me.player.IgnoreCollision(col,true);
			released = false;
		}

		void Start() {
			SetBrickKinematic();
		}

		void FixedUpdate() {
			if (stop || !released) return;
			if (waitIgnore > 0) {
				waitIgnore--;
				if (waitIgnore == 0) {
					col.isTrigger = false;
					gameObject.layer = brickLayerIndex;
					stop = true;
				}
				return;
			}
			bool adasdas = false;
			if (rb.velocity.sqrMagnitude < .001f && rb.angularVelocity < .1f) {
				waitDelta++;
				if (waitDelta >= 5) {
					waitDelta = 0;
					adasdas = true;
				}
			} else {
				waitDelta = 0;
			}
			if (adasdas || rb.IsSleeping()) {
				if (rend.bounds.min.x < Level.me.positionMin || rend.bounds.max.x > Level.me.positionSize+Level.me.positionMin) {
					Die();
				} else {
					rb.isKinematic = true;
					col.isTrigger = true;
					Level.me.bricks.Add(this);
					Level.me.player.IgnoreCollision(col,false);
					waitIgnore = 2;
				}
			}
		}

		void Update() {
			if (stop) {
				rend.material.color = Color.Lerp(rend.material.color,Color.black,Time.deltaTime*16);
			}
		}

		public void Die() {
			if (stop) return;
			Level.me.trashedBricks++;
			if (Level.me.brick == this) {
				Level.me.ReleaseBrick();
			}
			Level.me.Dust(rend.bounds.center,rend.bounds.size,(int)(Mathf.Max(1,rend.bounds.size.x*5)*Mathf.Max(1,rend.bounds.size.y*5)));
			Destroy(gameObject);
		}

		void OnCollisionEnter2D(Collision2D col) {
			if (stop) return;
			Level.me.CollisionEffect(col,rb.mass);
		}
		
		void OnTriggerStay2D(Collider2D col) {
			if (stop) return;
			if (col.name == "player") waitIgnore = 2;
		}

		public void SetBrickShape(BrickShape shape) {
			this.shape = shape;
			const float pixel = 1f/128f;
			const float mass = 5;
			sprite.localScale = Vector3.one;
			if (shape == BrickShape.Triangular || shape == BrickShape.TriangularAlt) {
				tr.localScale = Vector3.one;
				if (shape == BrickShape.Triangular) {
					col.points = new Vector2[] {
						new Vector2(-.5f,-.5f),
						new Vector2(-.5f,.5f),
						new Vector2(.5f,-.5f)
					};
				} else {
					sprite.localScale = new Vector3(-1,1,1);
					col.points = new Vector2[] {
						new Vector2(-.5f,-.5f),
						new Vector2(.5f,.5f),
						new Vector2(.5f,-.5f)
					};
				}
				rend.material.mainTextureOffset = new Vector2(.5f+pixel,pixel);
				rend.material.mainTextureScale = new Vector2(.5f-pixel*2,.5f-pixel*2);
				rb.mass = mass*.5f;
			} else {
				if (shape == BrickShape.Rectangular) {
					tr.localScale = new Vector3(2,1,1);
					rend.material.mainTextureOffset = new Vector2(pixel,.5f+pixel);
					rend.material.mainTextureScale = new Vector2(1-pixel*2,.5f-pixel*2);
					rb.mass = mass*2;
				} else {
					tr.localScale = Vector3.one;
					rend.material.mainTextureOffset = new Vector2(pixel,pixel);
					rend.material.mainTextureScale = new Vector2(.5f-pixel*2,.5f-pixel*2);
					rb.mass = mass;
				}
				col.points = new Vector2[] {
					new Vector2(-.5f,-.5f),
					new Vector2(-.5f,.5f),
					new Vector2(.5f,.5f),
					new Vector2(.5f,-.5f)
				};
			}
		}

		public void SetBrickTexture(string texture) {
			this.texture = texture;
			Texture2D tex;
			if (!textureByName.TryGetValue(texture,out tex)) tex = null;
			rend.material.mainTexture = tex;
		}
		
		public void SetBrickTexture(Texture2D texture) {
			this.texture = texture.name;
			rend.material.mainTexture = texture;
		}
		
		public void ReleaseBrick(bool doForce,Vector2 force) {
			released = true;
			col.enabled = true;
			rb.isKinematic = false;
			if (doForce) rb.AddForce(force*rb.mass,ForceMode2D.Impulse);
			rend.transform.localPosition = new Vector3(rend.transform.localPosition.x,rend.transform.localPosition.y,.9f);
		}

		public void SetBrickKinematic() {
			rb.isKinematic = Level.me.brickKinematic;
			col.enabled = !Level.me.brickKinematic;
		}
	}

	public enum BrickShape {
		Square,
		Rectangular,
		Triangular,
		TriangularAlt,
	}
}