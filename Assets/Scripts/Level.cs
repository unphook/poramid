﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Poramid {
	public class Level:MonoBehaviour {
		public static Level me { get; private set; }
		public Transform tr { get; private set; }

		public static LevelData data = null;

		public Base @base { get; private set; }
		public Player player { get; private set; }
		
		public Brick brick { get; private set; }
		public List<Brick> bricks { get; private set; }
		public int trashedBricks;
		public bool brickKinematic { get; private set; }

		AudioSource aud;
		public AudioSource audSfx { get; private set; }
		Transform camTr;
		Camera cam;
		Transform parallax;
		Transform dontRight;
		Transform limitLeft,limitRight;
		Transform dustTr;
		ParticleSystem dust;
		Renderer pyramidOverlayLeft,pyramidOverlayRight;

		Transform ui;
		Renderer holdToEnd;
		Transform timerTr;
		TextMesh timerText;
		Renderer fadeRend;
		public TextMesh instructions { get; private set; }

		GameObject brickPrefab;

		public const float positionMargin = 1;
		public float positionMin { get; private set; }
		public float positionSize { get; private set; }

		public Vector2 cameraPos { get; private set; }
		public Transform cameraFollow { get; private set; }
		public float cameraZoom { get; private set; }
		public float cameraVel { get; private set; }
		Vector2 cameraPosLerp;
		float cameraZoomLerp;

		public int state { get; private set; }
		public bool isPlaying {
			get {
				return state == 1;
			}
		}
		public float timer { get; private set; }
		float holdTimer;
		float fade;
		bool closing;
		bool threeSeconds;
		bool tenSeconds;

		void Awake() {
			me = this;
			tr = transform;
			
			brickPrefab = Resources.Load<GameObject>("prefabs/brick");
			@base = tr.Find("base").GetComponent<Base>();
			player = null;
			brick = null;
			bricks = new List<Brick>();
			trashedBricks = 0;

			aud = GetComponent<AudioSource>();
			audSfx = gameObject.AddComponent<AudioSource>();
			camTr = tr.Find("cam");
			cam = camTr.GetComponent<Camera>();
			parallax = tr.Find("parallax");
			dontRight = tr.Find("dontright");
			dontRight.name = "dont";
			limitLeft = tr.Find("limitleft");
			limitRight = tr.Find("limitright");
			dustTr = tr.Find("dust");
			dust = dustTr.GetComponent<ParticleSystem>();
			pyramidOverlayLeft = tr.Find("pyramidoverlayleft").GetComponent<Renderer>();
			pyramidOverlayRight = tr.Find("pyramidoverlayright").GetComponent<Renderer>();

			ui = tr.Find("ui");
			holdToEnd = ui.Find("hold").GetComponent<Renderer>();
			timerTr = ui.Find("timer");
			timerText = timerTr.Find("text").GetComponent<TextMesh>();
			fadeRend = ui.Find("fade").GetComponent<Renderer>();
			fadeRend.enabled = true;
			instructions = ui.Find("instructions").GetComponent<TextMesh>();

			holdTimer = 0;
			fade = 0;
			closing = false;
			threeSeconds = false;
			tenSeconds = false;
		}

		void Start() {
			positionMin = 2;
			positionSize = data.height*2+2;
			limitLeft.localPosition = new Vector3(positionMin,limitLeft.localPosition.y,limitLeft.localPosition.z);
			limitRight.localPosition = new Vector3(positionMin+positionSize,limitLeft.localPosition.y,limitLeft.localPosition.z);
			dontRight.localPosition = new Vector3(positionMin+positionSize+6,-1,0);
			float hypot = Mathf.Sqrt(2)*data.height;
			pyramidOverlayLeft.transform.localScale = pyramidOverlayRight.transform.localScale = new Vector3(hypot,.05f,1);
			pyramidOverlayLeft.transform.localPosition = new Vector3(positionMin+data.height*.5f+positionMargin,data.height*.5f-.5f,-5);
			pyramidOverlayRight.transform.localPosition = new Vector3(positionMin+data.height*1.5f+positionMargin,data.height*.5f-.5f,-5);
			pyramidOverlayLeft.material.mainTextureScale = pyramidOverlayRight.material.mainTextureScale = new Vector2(Mathf.Round(hypot*2),1);
			
			timerTr.localPosition = new Vector2(-Window.prop*5,6);

			state = 0;
			timer = 4;
			UpdatePlayer(true);
			SetCamera(player.tr,Vector2.zero,2,0);
			SetTimerText(data.duration);
			audSfx.volume = .5f;
			audSfx.loop = false;
			audSfx.clip = Game.audEnd;
			var preview = ui.Find("preview");
			float h = data.height*2.5f/7f;
			preview.localScale = new Vector3(h*2,h,1);
			preview.localPosition = new Vector3(8.3f-h,-4.5f+h*.5f,0);
			preview.GetComponent<Renderer>().material.mainTexture = Resources.Load<Texture2D>("sprites/pyramids/"+data.number);
		}

		void Update() {
			if (closing) {
				if (fade > 0) {
					fade -= Time.deltaTime;
					if (fade <= 0) {
						fade = 0;
						UnityEngine.SceneManagement.SceneManager.LoadScene("levelpreview");
						LevelPreview.ending = true;
					}
					fadeRend.material.color = new Color(0,0,0,1-fade);
				}
			} else {
				if (fade < 1) {
					fade += Time.deltaTime;
					if (fade > 1) fade = 1;
					fadeRend.material.color = new Color(0,0,0,1-fade);
				}
			}
			pyramidOverlayLeft.material.mainTextureOffset = pyramidOverlayRight.material.mainTextureOffset = new Vector2(-Time.time,0);
			if (state == 0) {
				timer -= Time.deltaTime;
				if (!threeSeconds && timer <= 3) {
					threeSeconds = true;
					audSfx.PlayOneShot(Game.audStart,.5f);
				}
				timerTr.localPosition = new Vector2(-Window.prop*5,5+Tween.EaseIn(timer-2));
				if (timer <= 0) {
					state = 1;
					timer = data.duration;
					aud.Play();
				}
			} else if (state == 1) {
				timer -= Time.deltaTime;
				if (!tenSeconds && timer <= 10) {
					tenSeconds = true;
					audSfx.Play();
				}
				if (timer <= 0) {
					SetTimerText(0);
					End();
				} else {
					SetTimerText(timer);
				}
				if (In.Hold(KeyCode.Escape)) {
					holdTimer += Time.deltaTime;
					if (holdTimer >= 1) {
						holdTimer = 0;
						audSfx.time = 10;
						audSfx.Play();
						End();
					}
				} else {
					holdTimer = 0;
				}
			} else if (state == 2) {
				if (timer > 0) {
					timer -= Time.deltaTime;
					if (timer < 0) timer = 0;
					timerTr.localPosition = new Vector2(-Window.prop*5,6-Tween.EaseOut(timer));
				}
			}
			holdToEnd.enabled = holdTimer > 0;
		}

		void SetTimerText(float timer) {
			int t = Mathf.CeilToInt(timer);
			timerText.text = string.Format("{0}:{1:D2}",t/60,t%60);
		}

		void End() {
			state = 2;
			timer = 1;
			aud.Stop();
			LevelPreview.rateLevel = new RateLevel();
			LevelPreview.rateLevel.Rate();
			closing = true;
		}

		void LateUpdate() {
			Vector2 pos;
			if (cameraFollow != null) {
				pos = (Vector2)cameraFollow.position+cameraPos;
			} else {
				pos = cameraPos;
			}
			if (cameraVel <= 0) {
				cameraPosLerp = pos;
				cameraZoomLerp = cameraZoom;
			} else {
				cameraPosLerp = Vector2.Lerp(cameraPosLerp,pos,Time.deltaTime*cameraVel);
				cameraZoomLerp = Mathf.Lerp(cameraZoomLerp,cameraZoom,Time.deltaTime*cameraVel);
			}
			camTr.localPosition = cameraPosLerp;
			cam.orthographicSize = cameraZoomLerp;
			parallax.localPosition = new Vector3(cameraPosLerp.x*.5f,cameraPosLerp.y*.5f,9);
		}

		public void SpawnBrick() {
			brick = Instantiate(brickPrefab).GetComponent<Brick>();
			brick.tr.parent = tr;
			brick.tr.localPosition = Vector2.zero;
			brick.SetBrickShape((BrickShape)(((int)(Random.value*4))%4));
			brick.SetBrickTexture(@base.texture);
			player.UpdateBrickPosition();
		}

		public void ReleaseBrick() {
			if (brick != null) {
				brick.ReleaseBrick(false,Vector2.zero);
				brick = null;
			}
		}
		
		public void ReleaseBrick(Vector2 force) {
			if (brick != null) {
				brick.ReleaseBrick(true,force);
				brick = null;
			}
		}

		public void UpdatePlayer(bool start = false) {
			if (player != null && @base.playerType == player.type) return;
			string path;
			switch (@base.playerType) {
				case PlayerType.Primitive: path = "prefabs/playerprimitive"; break;
				case PlayerType.Rope: path = "prefabs/playerrope"; break;
				case PlayerType.Catapult: path = "prefabs/playercatapult"; break;
				case PlayerType.Ufo: path = "prefabs/playerufo"; break;
				default: return;
			}
			if (player != null) {
				Destroy(player.gameObject);
			}
			player = Instantiate(Resources.Load<GameObject>(path)).GetComponent<Player>();
			player.tr.parent = tr;
			if (start) {
				player.tr.localPosition = Vector2.zero;
			} else if (@base.playerType == PlayerType.Ufo) {
				player.tr.localPosition = new Vector3(@base.tr.localPosition.x,@base.tr.localPosition.y+1,@base.tr.localPosition.z);
			} else {
				player.tr.localPosition = @base.tr.localPosition;
			}
			player.name = "player";
		}

		public void SetCamera(float left,float right,float vel = 5) {
			cameraFollow = null;
			cameraZoom = (right-left)*.5f/Window.prop;
			cameraPos = new Vector2((left+right)*.5f,cameraZoom*.9f-1);
			cameraVel = vel;
		}

		public void SetCamera(Vector2 pos,float zoom,float vel = 5) {
			cameraFollow = null;
			cameraPos = pos;
			cameraZoom = zoom;
			cameraVel = vel;
		}

		public void SetCamera(Transform obj,Vector2 offset,float zoom,float vel = 5) {
			cameraFollow = obj;
			cameraPos = offset;
			cameraZoom = zoom;
			cameraVel = vel;
		}

		public void SetCamera(float minX,float maxX,float minY,float maxY,float vel = 5) {
			cameraFollow = null;
			cameraPos = new Vector2((minX+maxX)*.5f,(minY+maxY)*.5f);
			float width = maxX-minX;
			float height = maxY-minY;
			float prop = width/height;
			if (Window.prop > prop) {
				cameraZoom = height*.5f;
			} else {
				cameraZoom = width*.5f/Window.prop;
			}
		}

		public void Dust(Vector2 pos,Vector2 size,int count,float duration = .4f) {
			dustTr.localPosition = new Vector3(pos.x,pos.y,dustTr.localPosition.z);
			var shape = dust.shape;
			shape.scale = size;
			dust.Emit(count);
		}

		public void CollisionEffect(Collision2D col,float mass) {
			if (col.transform.name == "dont") return;
			var force = col.relativeVelocity.magnitude*mass;
			float minX = col.contacts[0].point.x;
			float maxX = col.contacts[0].point.x;
			float minY = col.contacts[0].point.y;
			float maxY = col.contacts[0].point.y;
			for (int a = 1; a < col.contacts.Length; a++) {
				if (minX > col.contacts[a].point.x) {
					minX = col.contacts[a].point.x;
				} else if (maxX < col.contacts[a].point.x) {
					maxX = col.contacts[a].point.x;
				}
				if (minY > col.contacts[a].point.y) {
					minY = col.contacts[a].point.y;
				} else if (maxY < col.contacts[a].point.y) {
					maxY = col.contacts[a].point.y;
				}
			}
			var size = new Vector2(maxX-minX,maxY-minY);
			Dust(new Vector2((minX+maxX)*.5f,(minY+maxY)*.5f),size,(int)(Mathf.Max(1,mass/15f)*(Mathf.Max(1,size.x*10)*Mathf.Max(1,size.y*10))),Mathf.Clamp(mass/20f+.2f,.4f,.8f));
			int index;
			if (force > 20) {
				index = 2;
			} else if (force > 10) {
				index = 1;
			} else {
				index = 0;
			}
			audSfx.PlayOneShot(Game.audImpact[index],1);
		}

		public void SetBrickKinematic(bool k) {
			if (brickKinematic == k) return;
			brickKinematic = k;
			if (brick != null) {
				brick.SetBrickKinematic();
			}
		}
	}
}