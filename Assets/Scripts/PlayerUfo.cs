﻿using UnityEngine;
using System.Collections;

namespace Poramid {
	public class PlayerUfo:MonoBehaviour {
		Player player;
		Transform spriteTr;
		Renderer sprite;
		AudioSource aud;

		float rot;

		static Texture2D[] sprites = null;

		void Awake() {
			player = GetComponent<Player>();
			player.type = PlayerType.Ufo;
			spriteTr = player.tr.Find("sprite");
			sprite = spriteTr.GetComponent<Renderer>();
			aud = GetComponent<AudioSource>();

			rot = 0;

			if (sprites == null) {
				sprites = new Texture2D[] {
					Resources.Load<Texture2D>("sprites/player/ufo0"),
					Resources.Load<Texture2D>("sprites/player/ufo1"),
				};
			}
		}

		void Start() {
			Level.me.SetBrickKinematic(true);
			Level.me.instructions.text = "KHogZHJvcHMpIHVuaXR5IGtleWNvZGUgbGV0dGVyIGludCBtb2QgNiAwIC14IDEgK3ggMiAteSAzICt5IDQgK3JvdCA1IC1yb3Qg";
			player.limitVelocityX = false;
		}

		void Update() {
			if (!Level.me.isPlaying || Level.me.@base.open) return;
			if (player.teleportAlphaUpdate) {
				sprite.material.color = new Color(1,1,1,player.teleportAlpha);
			}
			if (player.teleporting) return;
			int axisX = 0;
			int axisY = 0;
			for (int a = (int)KeyCode.A; a < (int)KeyCode.Z; a++) {
				if (!In.Hold((KeyCode)a)) continue;
				int d = a%6;
				const float rotVel = 60;
				switch (d) {
					case 0: axisX--; break;
					case 1: axisX++; break;
					case 2: axisY--; break;
					case 3: axisY++; break;
					case 4: rot += Time.deltaTime*rotVel; break;
					case 5: rot -= Time.deltaTime*rotVel; break;
				}
			}
			if (axisX < -1) axisX = -1; else if (axisX > 1) axisX = 1;
			if (axisY < -1) axisY = -1; else if (axisY > 1) axisY = 1;
			player.rb.AddForce(new Vector2(axisX,axisY)*Time.deltaTime*200,ForceMode2D.Force);
			if (Level.me.brick != null) {
				Level.me.brick.tr.localRotation = Quaternion.Euler(0,0,rot);
				if (In.Press(KeyCode.Z)) {
					if (Level.me.brick.tr.localPosition.x >= Level.me.positionMin && Level.me.brick.tr.localPosition.x <= Level.me.positionMin+Level.me.positionSize) {
						Level.me.ReleaseBrick();
					} else {
						Level.me.brick.Die();
					}
				}
			} else {
				rot = 0;
			}
			Level.me.SetCamera(spriteTr,new Vector2(0,-1),4);
		}

		void FixedUpdate() {
			float mag = player.rb.velocity.magnitude;
			aud.pitch = .5f+mag*.1f;
			aud.volume = Mathf.Clamp01(mag*.5f)*.2f;
			if (Mathf.Abs(player.rb.velocity.x) > .2f && Mathf.Sign(spriteTr.localScale.x) != Mathf.Sign(player.rb.velocity.x)) {
				spriteTr.localScale = new Vector3(-spriteTr.localScale.x,spriteTr.localScale.y,spriteTr.localScale.z);
			}
		}

		void LateUpdate() {
			if (Level.me.brick != null) {
				UpdateBrickPosition(false);
			}
			sprite.material.mainTexture = sprites[(int)(Time.time*4)%2];
		}

		public void UpdateBrickPosition(bool rotate = true) {
			var height = Level.me.brick.tr.position.y-Level.me.brick.rend.bounds.min.y;
			Level.me.brick.tr.localPosition = new Vector3(spriteTr.position.x,Mathf.Max(spriteTr.position.y-1.25f,height-.5f),Level.me.brick.tr.localPosition.z);
			Level.me.brick.tr.rotation = Quaternion.Euler(0,0,rot);
		}
	}
}