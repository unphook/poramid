﻿using UnityEngine;
using System.Collections;

namespace Poramid {
	public class PlayerCatapult:MonoBehaviour {
		Player player;
		Transform spriteTr;
		Renderer sprite;
		Transform throwingTr;
		Renderer throwing;
		Transform directionTr;
		Renderer direction;
		AudioSource aud;
		Transform parabolaTr;
		Transform[] parabolaPathTr;
		Renderer[] parabolaPath;
		
		float anim;
		float angle;
		float force;
		float dir;

		const float minForce = 2;
		const float maxForce = 4;
		const float velForce = 1.5f;
		const float minAngle = 0;
		const float maxAngle = 90;
		const float velAngle = 30;
		const float dropForce = 5;

		Vector2 endForce;

		const int paths = 16;

		void Awake() {
			player = GetComponent<Player>();
			player.type = PlayerType.Catapult;
			spriteTr = player.tr.Find("sprite");
			sprite = spriteTr.GetComponent<Renderer>();
			throwingTr = player.tr.Find("throwing");
			throwing = throwingTr.GetComponent<Renderer>();
			directionTr = player.tr.Find("direction");
			direction = directionTr.GetComponent<Renderer>();
			aud = GetComponent<AudioSource>();
			parabolaTr = player.tr.Find("parabola");
			parabolaPathTr = new Transform[paths];
			parabolaPath = new Renderer[paths];
			for (int a = 0; a < paths; a++) {
				parabolaPathTr[a] = parabolaTr.Find(a.ToString());
				parabolaPath[a] = parabolaPathTr[a].GetComponent<Renderer>();
			}

			anim = 0;
			angle = 60;
			force = 2.5f;
			dir = 1;
			SetForce();
		}

		void Start() {
			Level.me.SetBrickKinematic(true);
			Level.me.instructions.text = "ARROWS moves, Z drops a brick, X+ARROWS sets the aim, SHIFT+ARROWS teleports, ESC ends";
			aud.volume = 0;
		}

		void SetForce() {
			endForce = new Vector2(Mathf.Cos(angle*Mathf.Deg2Rad)*force*dropForce,Mathf.Sin(angle*Mathf.Deg2Rad)*force*dropForce);
			directionTr.localScale = new Vector3(force,.125f,1);
			directionTr.localRotation = Quaternion.Euler(0,0,angle);
			float x0 = 0,x1 = 0,y0 = 0,y1 = 0;
			float step = endForce.y/(5*paths);
			for (int a = -1; a < paths; a++) {
				x0 = x1;
				y0 = y1;
				float t = (a+1)*step;
				x1 = endForce.x*t;
				y1 = t*(endForce.y-5*t);
				if (a < 0) continue;
				float dx = x1-x0;
				float dy = y1-y0;
				float mag = Mathf.Sqrt(dx*dx+dy*dy);
				parabolaPathTr[a].localPosition = new Vector3((x0+x1)*.5f,(y0+y1)*.5f,parabolaPathTr[a].localPosition.z);
				parabolaPathTr[a].localRotation = Quaternion.Euler(0,0,Mathf.Atan2(dy,dx)*Mathf.Rad2Deg);
				parabolaPathTr[a].localScale = new Vector3(mag,.05f,1);
				parabolaPath[a].material.mainTextureScale = new Vector2(mag*2,1);
			}
		}
		
		void Update() {
			if (!Level.me.isPlaying || Level.me.@base.open) return;
			if (player.teleportAlphaUpdate) {
				sprite.material.color = new Color(1,1,1,player.teleportAlpha);
				throwing.material.color = new Color(1,1,1,player.teleportAlpha);
				direction.material.SetColor("_TintColor",new Color(1,1,1,player.teleportAlpha*.2f));
			}
			if (player.teleporting) return;
			bool noise = false;
			if (In.Hold(KeyCode.X)) {
				float newAngle = Mathf.Clamp(angle-In.Axis(KeyCode.LeftArrow,KeyCode.RightArrow)*Time.deltaTime*velAngle*dir,minAngle,maxAngle);
				float newForce = Mathf.Clamp(force+In.Axis(KeyCode.DownArrow,KeyCode.UpArrow)*Time.deltaTime*velForce,minForce,maxForce);
				if (!Mathf.Approximately(angle,newAngle) || !Mathf.Approximately(force,newForce)) {
					angle = newAngle;
					force = newForce;
					SetForce();
				}
			} else {
				float axis = In.Axis(KeyCode.LeftArrow,KeyCode.RightArrow);
				if (axis != 0) {
					if (player.tr.localPosition.x < Level.me.positionMin) {
						player.Move(axis);
						if (player.tr.localPosition.x > Level.me.positionMin-.5f) {
							player.tr.localPosition = new Vector3(Level.me.positionMin-.5f,player.tr.localPosition.y,player.tr.localPosition.z);
						}
					} else {
						player.Move(axis);
						if (player.tr.localPosition.x < Level.me.positionMin+Level.me.positionSize+.5f) {
							player.tr.localPosition = new Vector3(Level.me.positionMin+Level.me.positionSize+.5f,player.tr.localPosition.y,player.tr.localPosition.z);
						}
					}
					noise = true;
				}
			}
			const float volume = .5f;
			if (noise) {
				if (aud.volume < volume) {
					if (!aud.isPlaying) aud.Play();
					aud.volume += Time.deltaTime*8/volume;
					if (aud.volume > volume) aud.volume = volume;
				}
			} else {
				if (aud.volume > 0) {
					aud.volume -= Time.deltaTime*8/volume;
					if (aud.volume <= 0) {
						aud.volume = 0;
						aud.Stop();
					}
				}
			}
			if (Level.me.brick != null && In.Release(KeyCode.Z)) {
				Level.me.ReleaseBrick(new Vector2(dir*endForce.x,endForce.y));
				Level.me.audSfx.PlayOneShot(Game.audDrop);
				anim = 1.5f;
			}
			Level.me.SetCamera(Level.me.@base.tr.localPosition.x-1.5f,Level.me.positionMin+Level.me.positionSize+4);
		}
		
		void LateUpdate() {
			dir = (player.tr.localPosition.x > Level.me.positionMin) ? -1 : 1;
			if (Level.me.brick != null) {
				Level.me.brick.rb.isKinematic = true;
				UpdateBrickPosition();
			}
			float offset = -Time.time;
			for (int a = 0; a < paths; a++) {
				parabolaPath[a].material.mainTextureOffset = new Vector2(offset,0);
				offset += parabolaPath[a].material.mainTextureScale.x;	
			}
			if (Mathf.Sign(player.tr.localScale.x) != dir) {
				player.tr.localScale = new Vector3(-player.tr.localScale.x,player.tr.localScale.y,player.tr.localScale.z);
			}
			if (anim > 0) {
				anim -= Time.deltaTime*2;
				if (anim < 0) anim = 0;
			}
			if (anim > 1.25f) {
				throwingTr.localRotation = Quaternion.Euler(0,0,-45*Tween.EaseOut(Mathf.InverseLerp(1.5f,1.25f,anim)));
			} else {
				throwingTr.localRotation = Quaternion.Euler(0,0,-45*Tween.Ease(Mathf.InverseLerp(0,1.25f,anim)));
			}
		}

		public void UpdateBrickPosition() {
			Level.me.brick.tr.localPosition = new Vector3(spriteTr.position.x-dir*1.2f,spriteTr.position.y-.2f,Level.me.brick.tr.localPosition.z);
			Level.me.brick.tr.localRotation = Quaternion.identity;
			var brickPos = Level.me.brick.tr.localPosition-player.tr.localPosition;
			directionTr.localPosition = new Vector3(brickPos.x*dir,brickPos.y,-directionTr.localPosition.z);
			parabolaTr.localPosition = new Vector3(brickPos.x*dir,brickPos.y,-parabolaTr.localPosition.z);
		}
	}
}