﻿using UnityEngine;
using System.Collections;

namespace Poramid {
	public class Game:MonoBehaviour {
		public static Game me { get; private set; }

		public LevelData[] levels { get; private set; }
		
		public static AudioClip audStart,audEnd;
		public static AudioClip[] audImpact;
		public static AudioClip audBack,audScroll,audSelect;
		public static AudioClip audBase;
		public static AudioClip audDrop;

		void LoadSounds() {
			audStart = Resources.Load<AudioClip>("audio/start");
			audEnd = Resources.Load<AudioClip>("audio/end");
			audImpact = new AudioClip[] {
				Resources.Load<AudioClip>("audio/impact0"),
				Resources.Load<AudioClip>("audio/impact1"),
				Resources.Load<AudioClip>("audio/impact2")
			};
			audBack = Resources.Load<AudioClip>("audio/back");
			audScroll = Resources.Load<AudioClip>("audio/scroll");
			audSelect = Resources.Load<AudioClip>("audio/select");
			audBase = Resources.Load<AudioClip>("audio/base");
			audDrop = Resources.Load<AudioClip>("audio/drop");
		}

		void Awake() {
			if (me != null) {
				Destroy(gameObject);
				return;
			}
			DontDestroyOnLoad(gameObject);
			me = this;
			Window.Start();
			LoadSounds();

			levels = new LevelData[10];
			int i = 0;

			levels[i] = new LevelData(i,2,2);
			levels[i].client = "Mummey";
			levels[i].request =
@"Hey dude. I've got some hard-earned money and I want
to live in a pyramid, throw parties and hang out with my
buddies. Do your thing, I guess?";
			levels[i].positive =
@"Bro... that looks pretty good. You got my respect. Come
on, let's go to my party!";
			levels[i].negative =
@"That was terrible. You don't know who you just tricked.
I'll spare you because I'm in a good mood, so be thankful.";
			i++;

			levels[i] = new LevelData(i,3,2.5f);
			levels[i].decoration.Add(new Decoration("red",1));
			levels[i].client = "Dhaat Fella";
			levels[i].request =
@"Hello. I liked the pyramid you built for Mummey and it
would be cool if you built one for me too. Can we go to
the place you'll build? I want it for today... Please.";
			levels[i].positive =
@"Dude, I was expecting something good, but not THAT
good! I'll tell Chloe about you and your job. You're
cordially invited!";
			levels[i].negative =
@"I thought you were good at this, but that was a bummer.
Looks like you'll be jailed, too.";
			i++;

			levels[i] = new LevelData(i,3,3.5f);
			levels[i].decoration.Add(new Decoration("yellow",2));
			levels[i].decoration.Add(new Decoration("blue",1));
			levels[i].decoration.Add(new Decoration("yellow",0));
			levels[i].client = "Ducc";
			levels[i].request =
@"Qua-Hello! How are you, Onubie? Heh! All gooey with ya?
Now go build me that pyramid, oquack?";
			levels[i].positive =
@"QUACK! THIS PYRAMID IS REALLY GOOD! *quack* Honestly, I
had a pyramid already and it is poor compared to what
you just did. I'll live there forever. Thanks, Onubie!";
			levels[i].negative =
@"*weak quack* Accept your role as your father's successor
and become a god worthy of wealth and comfort.";
			i++;

			levels[i] = new LevelData(i,4,3.5f);
			levels[i].decoration.Add(new Decoration("eye",3.5f,2));
			levels[i].decoration.Add(new Decoration("green",0));
			levels[i].client = "Allye";
			levels[i].request =
@"YnVpbGQzNDY1MzQ2NTM4MDU3Njg0NXB5cmFtaWRweXJhbWlk
cHlyYW1pZEgxMjM3ODUyNDU0NzgzNTI";
			levels[i].positive =
@"dGhhbmsgeW91IHRoYW5rIHlvdSB0aGFuayB5b3U";
			levels[i].negative =
@"eW91IGxpdHRsZSBwaWVjZSBvZiBzaGl0";
			i++;

			levels[i] = new LevelData(i,4,4);
			levels[i].decoration.Add(new Decoration("blue",3));
			levels[i].decoration.Add(new Decoration("yellow",2));
			levels[i].decoration.Add(new Decoration("blue",1));
			levels[i].decoration.Add(new Decoration("yellow",0));
			levels[i].client = "Chloepatra";
			levels[i].request =
@"My dad gave me a handful of gold to build my own
pyramid and it'd be fine if you're the one to make it. Can
you use your skills of craftsmanship to build a majestic
pyramid?";
			levels[i].positive =
@"Marvelous! This pyramid looks even better than my
boyfriend's pyramid, and his pyramid is awesome.";
			levels[i].negative =
@"I knew Fella was wrong! I will tell my father and you'll be
EXECUTED, you peasant!";
			i++;

			levels[i] = new LevelData(i,5,5);
			levels[i].decoration.Add(new Decoration("brown",3));
			levels[i].decoration.Add(new Decoration("brown",1));
			levels[i].client = "Sphinx";
			levels[i].request =
@"Greetings, my young god. I am Sphinx, pleased to meet
you. I demand a pyramid worthy of my kindness.";
			levels[i].positive =
@"That is absolutely splendid, truly the work of a big,
outrageous god! I'm so happy, thank you so much.";
			levels[i].negative =
@"Oh... I'm sorry, but it looks completely disarrayed,
definitely doesn't show a thing about my beauty and
kindness.";
			i++;

			levels[i] = new LevelData(i,5,5.5f);
			levels[i].decoration.Add(new Decoration("yellow",4));
			levels[i].decoration.Add(new Decoration("blue",3));
			levels[i].decoration.Add(new Decoration("sun",3,2));
			levels[i].decoration.Add(new Decoration("sun",6,2));
			levels[i].decoration.Add(new Decoration("blue",1));
			levels[i].decoration.Add(new Decoration("yellow",0));
			levels[i].client = "Amon";
			levels[i].request =
@"Salutations, fellow god. I, Amon, would like to request your
abilities of craftsmanship to forge a mighty god pyramid.
Here is the necessary gold for such magnificent building.";
			levels[i].positive =
@"My, Onubis, this is such a lustrous pyramid! Its edges, its
sharpness and its aesthetics are out of this world! You
have my reverence.";
			levels[i].negative =
@"What a tragedy. I thought your crafts were worthwhile,
but I was wrong. You failed, Onubis. I'm really
discontented.";
			i++;

			levels[i] = new LevelData(i,6,6.5f);
			levels[i].decoration.Add(new Decoration("red",4));
			levels[i].decoration.Add(new Decoration("sun",4,3));
			levels[i].decoration.Add(new Decoration("sun",7,3));
			levels[i].decoration.Add(new Decoration("red",2));
			levels[i].client = "Haaa";
			levels[i].request =
@"You are Anubis' son, I assume? Your father told me about
the situation. You sparred everything away to pursue
something of your liking? Demonstrate your skills to
yours truly and I might just make up my mind.";
			levels[i].positive =
@"That was impressive. Very well, you have my approval.
This goes beyond most of the projects I have realized in
all my existence. You are destined to build beautiful
things... I have been enlightened. You have my gratitude.";
			levels[i].negative =
@"I knew it. You were a fluke ever since you were born.
Just quit and be your father's minion. You wasted my
precious time and resources.";
			i++;

			levels[i] = new LevelData(i,6,7);
			levels[i].decoration.Add(new Decoration("white",5));
			levels[i].decoration.Add(new Decoration("sun",5,4));
			levels[i].decoration.Add(new Decoration("sun",6,4));
			levels[i].decoration.Add(new Decoration("white",3));
			levels[i].decoration.Add(new Decoration("white",2));
			levels[i].decoration.Add(new Decoration("sun",2,1));
			levels[i].decoration.Add(new Decoration("sun",9,1));
			levels[i].decoration.Add(new Decoration("white",0));
			levels[i].client = "Mad Jed";
			levels[i].request =
@"Hello there, I wanna settle in this town and I want you to
make me a pyramid. I'm MAAAD. Here's the gold, by the
way.";
			levels[i].positive =
@"I'm not furious anymore, Onubis. You're good at this,
good job. By the way, I'm MAD right now.";
			levels[i].negative =
@"WHAT IS THAT? THAT IS NOT A PYRAMID.
G e t  o u t t a  m y  f a c e.";
			i++;

			levels[i] = new LevelData(i,7,8.5f);
			levels[i].decoration.Add(new Decoration("black",6));
			levels[i].decoration.Add(new Decoration("eye",6.5f,5));
			levels[i].decoration.Add(new Decoration("yellow",4));
			levels[i].decoration.Add(new Decoration("sun",5,3));
			levels[i].decoration.Add(new Decoration("sun",8,3));
			levels[i].decoration.Add(new Decoration("yellow",2));
			levels[i].decoration.Add(new Decoration("sun",3,1));
			levels[i].decoration.Add(new Decoration("sun",5,1));
			levels[i].decoration.Add(new Decoration("sun",8,1));
			levels[i].decoration.Add(new Decoration("sun",10,1));
			levels[i].decoration.Add(new Decoration("black",0));
			levels[i].client = "Anubis";
			levels[i].request =
@"Son, it's your father, Anubis. I felt remorseful after your
banishment. Now other gods are mesmerized by your
prodigious crafting skills. Show me you are worthy of
being my successor!";
			levels[i].positive =
@"I was mistaken, and now I am proud of you. You forged
the most astonishing thing I have ever seen in my entire
existence.";
			levels[i].negative =
@"I am disappointed. How shameful, you can't learn from
your father.";
			i++;
			
			Level.data = levels[0];
		}

		void Update() {
			Window.Update();
		}
	}
}