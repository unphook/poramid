﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Poramid {
	public static class CollisionSolver {
		public static bool Solve(Collision2D col,bool enter,Dictionary<Collider2D,bool> collisions,ref int groundCount) {
			bool oldValue;
			bool wasBefore = collisions.TryGetValue(col.collider,out oldValue);
			bool newValue = false;
			if (enter) {
				for (int a = 0; a < col.contacts.Length; a++) {
					if (col.contacts[a].normal.y >= .01f) {
						newValue = true;
						break;
					}
				}
				collisions[col.collider] = newValue;
			} else {
				if (wasBefore) collisions.Remove(col.collider);
				oldValue = false;
			}
			if (newValue) {
				if (!oldValue) groundCount++;
			} else {
				if (oldValue) groundCount--;
			}
			return newValue;
		}
	}
}
