﻿using UnityEngine;
using System.Collections;

namespace Poramid {
	public class PlayerPrimitive:MonoBehaviour {
		Player player;
		Transform spriteTr;
		Renderer sprite;
		Renderer hands;

		float rot;
		float currentRot;
		float anim;

		static Texture2D[] sprites = null;

		public const float vel = 2.5f;
		public const float velBrick = 1.5f;
		public const float jump = 5.7f;
		public const float jumpBrick = 4.7f;

		void Awake() {
			player = GetComponent<Player>();
			player.type = PlayerType.Primitive;
			spriteTr = player.tr.Find("sprite");
			sprite = spriteTr.GetComponent<Renderer>();
			hands = spriteTr.Find("hands").GetComponent<Renderer>();

			rot = 0;
			currentRot = 0;
			anim = 0;

			if (sprites == null) {
				sprites = new Texture2D[] {
					Resources.Load<Texture2D>("sprites/player/primitive0"),
					Resources.Load<Texture2D>("sprites/player/primitive1"),
					Resources.Load<Texture2D>("sprites/player/primitive2"),
					Resources.Load<Texture2D>("sprites/player/primitivebrick0"),
					Resources.Load<Texture2D>("sprites/player/primitivebrick1"),
					Resources.Load<Texture2D>("sprites/player/primitivebrick2"),
				};
			}
		}
		
		void Start() {
			Level.me.SetBrickKinematic(true);
			Level.me.instructions.text = "ARROWS moves, SPACE jumps, Z drops a brick, X rotates a brick, SHIFT+ARROWS teleports, ESC ends";
		}
		
		void Update() {
			if (!Level.me.isPlaying || Level.me.@base.open) return;
			if (player.teleportAlphaUpdate) {
				sprite.material.color = new Color(1,1,1,player.teleportAlpha);
				hands.material.color = new Color(1,1,1,player.teleportAlpha);
			}
			if (player.teleporting) return;
			float axis = In.Axis(KeyCode.LeftArrow,KeyCode.RightArrow);
			if (axis != 0) {
				float v = (Level.me.brick != null) ? velBrick : vel;
				player.Move(axis*v);
				anim = (anim+vel*Time.deltaTime*4)%4;
				if (Mathf.Sign(spriteTr.localScale.x) != Mathf.Sign(axis)) {
					spriteTr.localScale = new Vector3(-spriteTr.localScale.x,spriteTr.localScale.y,spriteTr.localScale.z);
				}
			} else {
				anim = 0;
			}
			if (player.grounded && In.Press(KeyCode.Space)) player.Jump((Level.me.brick != null) ? jumpBrick : jump);
			if (Level.me.brick != null) {
				if (In.Press(KeyCode.X)) {
					rot -= 90;
				}
				currentRot = Mathf.LerpAngle(currentRot,rot,Time.deltaTime*8);
				Level.me.brick.tr.rotation = Quaternion.Euler(0,0,currentRot);
				if (In.Press(KeyCode.Z)) {
					if (Level.me.brick.tr.localPosition.x >= Level.me.positionMin && Level.me.brick.tr.localPosition.x <= Level.me.positionMin+Level.me.positionSize) {
						Level.me.ReleaseBrick();
					} else {
						Level.me.brick.Die();
					}
				}
			} else {
				currentRot = rot = 0;
			}
			Level.me.SetCamera(spriteTr,new Vector2(0,1),3);
		}
		
		void LateUpdate() {
			if (Level.me.brick != null) {
				Level.me.brick.rb.isKinematic = true;
				UpdateBrickPosition(false);
			}
			hands.enabled = Level.me.brick != null;
			int animFrame;
			if (player.grounded) {
				switch ((int)anim) {
					case 1: animFrame = 1; break;
					case 3: animFrame = 2; break;
					default: animFrame = 0; break;
				}
			} else {
				animFrame = 1;
			}
			if (hands.enabled) animFrame += 3;
			sprite.material.mainTexture = sprites[animFrame];
		}

		public void UpdateBrickPosition(bool rotate = true) {
			var height = Mathf.Max(0,Level.me.brick.tr.position.y-Level.me.brick.rend.bounds.min.y);
			Level.me.brick.tr.localPosition = new Vector3(spriteTr.position.x,spriteTr.position.y+height-.1f,Level.me.brick.tr.localPosition.z);
			Level.me.brick.tr.rotation = Quaternion.Euler(0,0,currentRot);
		}
	}
}