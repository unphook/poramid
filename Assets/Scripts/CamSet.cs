﻿using UnityEngine;
using System.Collections;

namespace Poramid {
	public class CamSet:MonoBehaviour {
		public bool fullRect = true;
		public Rect rect = new Rect(0,0,1,1);
		Camera cam;

		void Start() {
			cam = GetComponent<Camera>();
			if (fullRect) {
				cam.rect = Window.fullRect;
			} else {
				cam.rect = Window.CamRect(rect);
			}
		}

		void LateUpdate() {
			if (Window.update) {
				if (fullRect) {
					cam.rect = Window.fullRect;
				} else {
					cam.rect = Window.CamRect(rect);
				}
			}
		}
	}
}