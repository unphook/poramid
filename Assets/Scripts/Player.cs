﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Poramid {
	public class Player:MonoBehaviour {
		public bool grounded { get; private set; }

		public Transform tr { get; private set; }
		public Rigidbody2D rb { get; private set; }
		public CircleCollider2D colCircle { get; private set; }
		public BoxCollider2D colBox { get; private set; }
		public Renderer teleport { get; private set; }

		Dictionary<Collider2D,bool> collisions;
		int groundCount;
		int groundTolerance;

		public PlayerType type;
		
		public bool teleporting { get; private set; }
		public float teleportAlpha { get; private set; }
		public bool teleportAlphaUpdate { get; private set; }
		int teleportPos;
		float teleportTempo;
		
		static int groundToleranceTotal = 2;

		public bool limitVelocityX;
		
		void Awake() {
			grounded = false;

			tr = transform;
			rb = GetComponent<Rigidbody2D>();
			colCircle = GetComponent<CircleCollider2D>();
			colBox = GetComponent<BoxCollider2D>();
			teleport = tr.Find("teleport").GetComponent<Renderer>();
			teleport.enabled = false;

			collisions = new Dictionary<Collider2D,bool>();
			groundCount = 0;
			groundTolerance = 0;

			teleporting = false;
			teleportAlpha = 1;
			teleportAlphaUpdate = false;
			teleportPos = 0;
			teleportTempo = 0;

			limitVelocityX = true;
		}

		void Start() {
			if (Level.me.brick != null) {
				IgnoreCollision(Level.me.brick.col,true);
				UpdateBrickPosition();
			}
		}

		void Update() {
			if (!teleporting && Level.me.isPlaying && !Level.me.@base.open) {
				if (tr.localPosition.x >= Level.me.positionMin && In.Press(KeyCode.LeftArrow) && (In.Hold(KeyCode.LeftShift) || In.Hold(KeyCode.RightShift))) {
					teleporting = true;
					teleportTempo = 1;
					teleportPos = -1;
				} else if (tr.localPosition.x <= Level.me.positionMin+Level.me.positionSize && In.Press(KeyCode.RightArrow) && (In.Hold(KeyCode.LeftShift) || In.Hold(KeyCode.RightShift))) {
					teleporting = true;
					teleportTempo = 1;
					teleportPos = 1;
				}
			}
			if (teleporting) {
				teleportAlphaUpdate = true;
				float bf = teleportTempo;
				teleportTempo -= Time.deltaTime*0.2f;
				if (bf > .5f && teleportTempo <= .5f) {
					tr.localPosition = new Vector2((teleportPos == -1) ? 0 : (Level.me.positionMin*2+Level.me.positionSize),0);
				}
				if (teleportTempo <= 0) {
					teleportTempo = 0;
					teleporting = false;
					teleport.enabled = false;
				} else {
					teleport.enabled = true;
				}
				float anim = 1-((1-Mathf.Abs(teleportTempo*2-1))*1.3f);
				teleportAlpha = Tween.Ease(anim);
				teleport.material.SetColor("_TintColor",new Color(0,1,1,-.36f*anim*(anim-1)));
				teleport.material.mainTextureOffset = new Vector2(0,Time.time);
			}
		}

		void FixedUpdate() {
			if (groundTolerance > 0) {
				groundTolerance--;
				if (groundTolerance == 0) {
					collisions.Clear();
					groundCount = 0;
					grounded = false;
				}
			}
			if (limitVelocityX) {
				rb.velocity = new Vector2(Mathf.Lerp(rb.velocity.x,0,Time.fixedDeltaTime*6),rb.velocity.y);
			}
		}
		
		void OnCollisionEnter2D(Collision2D col) {
			Level.me.CollisionEffect(col,rb.mass);
			if (CollisionSolver.Solve(col,true,collisions,ref groundCount)) {
				groundTolerance = groundToleranceTotal;
			}
			grounded = groundCount > 0;
		}

		void OnCollisionStay2D(Collision2D col) {
			if (CollisionSolver.Solve(col,true,collisions,ref groundCount)) {
				groundTolerance = groundToleranceTotal;
			}
			grounded = groundCount > 0;
		}

		void OnCollisionExit2D(Collision2D col) {
			CollisionSolver.Solve(col,false,collisions,ref groundCount);
			grounded = groundCount > 0;
		}

		public void Move(float velocity) {
			var delta = velocity*Time.deltaTime;
			tr.localPosition = new Vector3(tr.localPosition.x+delta,tr.localPosition.y,tr.localPosition.z);
		}

		public void Jump(float impulse) {
			rb.velocity = new Vector2(rb.velocity.x,impulse);
		}

		public void IgnoreCollision(Collider2D col,bool ignore) {
			//Physics2D.IgnoreCollision(col,colCircle,ignore);
			Physics2D.IgnoreCollision(col,colBox,ignore);
		}

		public void UpdateBrickPosition() {
			switch (type) {
				case PlayerType.Primitive: GetComponent<PlayerPrimitive>().UpdateBrickPosition(); break;
				case PlayerType.Rope: GetComponent<PlayerRope>().UpdateBrickPosition(); break;
				case PlayerType.Catapult: GetComponent<PlayerCatapult>().UpdateBrickPosition(); break;
			}
		}
	}

	public enum PlayerType {
		Primitive,
		Rope,
		Catapult,
		Ufo,
	}
}