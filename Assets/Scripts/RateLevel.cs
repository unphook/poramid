﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Poramid {
	public class RateLevel {
		static float sin = Mathf.Sqrt(2)*.5f;
		static float cos = -Mathf.Sqrt(2)*.5f;
		static List<Vector2> points = new List<Vector2>();
		static List<int> validPoints = new List<int>();
		static List<Vector2> oldPoints = new List<Vector2>();
		static RaycastHit2D[] hits = new RaycastHit2D[64];
		static int layerMask = LayerMask.GetMask("Brick");

		public float areaScore;
		public float shapeScore;
		public float textureScore;
		public float total;
		public string grade;

		public void Rate() {
			//area score
			float bottom = -.5f;
			float top = Level.data.height+bottom;
			float centre = Level.me.positionMin+Level.data.height+Level.positionMargin;
			float areaInside = 0;
			float totalArea = 0;
			float optimalArea = Level.data.height*Level.data.height;
			var decBricks = new List<Brick>();
			for (int a = 0; a < Level.me.bricks.Count; a++) {
				if (Level.me.bricks[a].texture != "default") {
					decBricks.Add(Level.me.bricks[a]);
				}
				float shapeArea;
				int pointsLength;
				int skipPoint = 4;
				switch (Level.me.bricks[a].shape) {
					case BrickShape.Triangular: shapeArea = .5f; pointsLength = 3; break;
					case BrickShape.TriangularAlt: shapeArea = .5f; pointsLength = 3; skipPoint = 2; break;
					case BrickShape.Square: shapeArea = 1; pointsLength = 4; break;
					case BrickShape.Rectangular: shapeArea = 2; pointsLength = 4; break;
					default: return;
				}
				totalArea += shapeArea;
				bool check = false;
				for (int b = 0; b < pointsLength; b++) {
					int index = b;
					if (index >= skipPoint) index++;
					var pos = Level.me.bricks[a].tr.Find(index.ToString()).position;
					pos = new Vector2(pos.x-centre,pos.y-top);
					pos = new Vector2(pos.x*cos-pos.y*sin,pos.x*sin+pos.y*cos);
					points.Add(pos);
					if (pos.x < 0 || pos.y < 0) check = true;
				}
				float area;
				if (check) {
					CutPolygon(v => v.x >= 0,(u,v) => new Vector2(0,Mathf.Lerp(u.y,v.y,Mathf.InverseLerp(u.x,v.x,0))));
					CutPolygon(v => v.y >= 0,(u,v) => new Vector2(Mathf.Lerp(u.x,v.x,Mathf.InverseLerp(u.y,v.y,0)),0));
					area = 0;
					for (int b = 0; b < points.Count; b++) {
						var p0 = points[b];
						var p1 = points[(b+1)%points.Count];
						area += (p0.x+p1.x)*(p0.y-p1.y);
					}
					area *= .5f;
				} else {
					area = shapeArea;
				}
				areaInside += area;
				/*
				for (int b = 0; b < points.Count; b++) {
					var p0 = points[b];
					var p1 = points[(b+1)%points.Count];
					p0 = new Vector3(p0.x*cos+p0.y*sin+centre,-p0.x*sin+p0.y*cos+top,-9);
					p1 = new Vector3(p1.x*cos+p1.y*sin+centre,-p1.x*sin+p1.y*cos+top,-9);
					Debug.DrawLine(p0,p1,Color.red,60);
				}
				//*/
				points.Clear();
			}
			areaScore = (Level.me.bricks.Count == 0) ? 0 : (areaInside/optimalArea);
			
			//shape score
			int totalSideShapes = 0;
			int triangularSideShapes = 0;
			float y = bottom+.3f;
			FindShapes(y,(b0,b1) => b0.tr.localPosition.x < b1.tr.localPosition.x,Level.me.positionMin,Vector2.right,135,ref totalSideShapes,ref triangularSideShapes);
			FindShapes(y,(b0,b1) => b0.tr.localPosition.x > b1.tr.localPosition.x,Level.me.positionMin+Level.me.positionSize,Vector2.left,45,ref totalSideShapes,ref triangularSideShapes);
			shapeScore = (Level.me.bricks.Count == 0) ? 0 : (((areaInside/totalArea)+((totalSideShapes == 0) ? 0 : ((float)triangularSideShapes/totalSideShapes)))/2f);

			//texture scoree
			if (Level.data.decoration.Count == 0) {
				textureScore = -1;
			} else if (decBricks.Count == 0) {
				textureScore = 0;
			} else {
				int totalDecorationBricks = decBricks.Count;
				var dec = new List<RateDecoration>();
				for (int a = 0; a < Level.data.decoration.Count; a++) {
					dec.Add(new RateDecoration(Level.data.decoration[a]));
				}
				const float rotMax = 25;
				for (int a = 0; a < decBricks.Count; a++) {
					for (int b = 0; b < dec.Count; b++) {
						if (dec[b].data.isLine) {
							if (Mathf.Abs(decBricks[a].tr.localPosition.y-dec[b].data.y) > .5f) continue;
							if (dec[b].data.type != decBricks[a].texture) continue;
							if (Mathf.DeltaAngle(decBricks[a].tr.localRotation.eulerAngles.z*2,0) > rotMax*2) continue;
						} else {
							if (dec[b].progress >= dec[b].total) continue;
							if (Mathf.Abs(decBricks[a].tr.localPosition.x-.5f-Level.positionMargin-Level.me.positionMin-dec[b].data.x) > .7f) continue;
							if (Mathf.Abs(decBricks[a].tr.localPosition.y-dec[b].data.y) > .6f) continue;
							if (dec[b].data.type != decBricks[a].texture) continue;
							if (dec[b].data.type != "sun" && Mathf.DeltaAngle(decBricks[a].tr.localRotation.eulerAngles.z*2,0) > rotMax*2) continue;
						}
						float progress;
						switch (decBricks[a].shape) {
							case BrickShape.Triangular: progress = .5f; break;
							case BrickShape.TriangularAlt: progress = .5f; break;
							case BrickShape.Square: progress = 1; break;
							case BrickShape.Rectangular: progress = 2; break;
							default: continue;
						}
						dec[b].progress += progress;
						decBricks.RemoveAt(a--);
						break;
					}
				}
				float decTotal = 0;
				float decProgress = 0;
				for (int a = 0; a < dec.Count; a++) {
					decTotal += dec[a].total;
					decProgress += Mathf.Min(dec[a].progress*1.5f,dec[a].total);
				}
				textureScore = Mathf.Min(decProgress/decTotal,1-(float)decBricks.Count/totalDecorationBricks);
			}

			//totall
			if (textureScore < -.5f) {
				total = (areaScore+shapeScore)/2f;
			} else {
				total = (areaScore+shapeScore+textureScore)/3f;
			}
			if (total >= .96f) {
				grade = "A+";
			} else if (total >= .9f) {
				grade = "A";
			} else if (total >= .8f) {
				grade = "B";
			} else if (total >= .7f) {
				grade = "C";
			} else {
				grade = "F";
			}
		}

		void CutPolygon(System.Func<Vector2,bool> valid,System.Func<Vector2,Vector2,Vector2> intersect) {
			for (int a = 0; a < points.Count; a++) {
				oldPoints.Add(points[a]);
				if (valid(points[a])) validPoints.Add(a);
			}
			points.Clear();
			for (int a = 0; a < validPoints.Count; a++) {
				int p0 = validPoints[a];
				points.Add(oldPoints[p0]);
				int p1 = validPoints[(a+1)%validPoints.Count];
				int d0 = (p0+1)%oldPoints.Count;
				int d1 = p1-1;
				if (d1 < 0) d1 += oldPoints.Count;
				if (p0 == d1 || p1 == d0) continue;
				var intersect0 = intersect(oldPoints[p0],oldPoints[d0]);
				var intersect1 = intersect(oldPoints[p1],oldPoints[d1]);
				if (intersect0 != oldPoints[p0]) points.Add(intersect0);
				if (intersect1 != oldPoints[p1]) points.Add(intersect1);
			}
			oldPoints.Clear();
			validPoints.Clear();
		}

		void FindShapes(float y,System.Func<Brick,Brick,bool> select,float from,Vector2 dir,float angle,ref int totalSideShapes,ref int triangularSideShapes) {
			const float rotMax = 20;
			while (true) {
				int c = Physics2D.RaycastNonAlloc(new Vector2(from,y),dir,hits,Level.me.positionSize,layerMask);
				Brick found = null;
				Vector2 normal = Vector2.zero;
				for (int b = 0; b < c; b++) {
					var bricc = hits[b].transform.GetComponent<Brick>();
					if (bricc == null) continue;
					if (found == null || select(bricc,found)) {
						found = bricc;
						normal = hits[b].normal;
					}
				}
				if (found == null) return;
				totalSideShapes++;
				float normalAngle = Mathf.Atan2(normal.y,normal.x)*Mathf.Rad2Deg;
				//Debug.Log(string.Format("{0} {1}",normalAngle,angle));
				if (Mathf.Abs(Mathf.DeltaAngle(angle,normalAngle)) < rotMax) {
					triangularSideShapes++;
				}
				y += found.rend.bounds.size.y;
			}
		}

		class RateDecoration {
			public Decoration data;
			public float total;
			public float progress;

			public RateDecoration(Decoration data) {
				this.data = data;
				progress = 0;
				total = data.isLine ? (Level.data.height*2-1-data.y*2) : 1;
			}
		}
	}
}